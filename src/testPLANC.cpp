#include "planc.hpp"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <time.h>
#include <adolc/adolc.h>

#define M_PI	 3.14159265358979323846

using namespace std;

PlanC create_simple() {
	size_t n = 1;
	size_t m = 1;
	size_t s = 0;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	Vector xo(n);
	J[0][0] = 2;
	b[0] = 1;

	return PlanC(c, b, Z, L, J, Y, xo);

}

void test_integration_simple() {
	size_t n = 1;
	size_t m = 1;
	size_t s = 0;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	Vector xo(n);
	xo[0] = -5;
	J[0][0] = 2;
	b[0] = 1;

	PlanC myPlan(c, b, Z, L, J, Y, xo);

	Vector v(1);
	v[0] = 0.5;
	cout << "v = " << v << endl;
	cout << "xo = " << xo << endl;
	cout << "int(v) = " << myPlan.integrate(v) << endl;
	cout << "int(-v) = " << myPlan.integrate(-v) << endl;
	cout << "fxo = " << myPlan.eval(xo) << endl;
	cout << "r = "
			<< myPlan.integrate(v) + myPlan.integrate(-v) - myPlan.eval(xo)
			<< endl;
}

PlanC create_brugnano(size_t N) {
	size_t n = N;
	size_t m = N;
	size_t s = N;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	Vector xo(n);

	//Brugnano
	for (size_t i = 0; i < N; ++i) {
		b[i] = -1;
		for (size_t j = 0; j < N; ++j) {
			Z[i][j] = i == j ? 1 : 0;
			Y[N - 1 - i][j] = i == j ? 0.5 : 0;
		}
	}

	for (size_t i = 0; i < N - 1; ++i) {
		J[N - 1 - i][i] = 2.5;
		J[N - 1 - i - 1][i] = -1;
		J[N - 1 - i][i + 1] = -1;
	}
	J[0][N - 1] = 2.5;

	return PlanC(c, b, Z, L, J, Y, xo);

}

void test_integration(size_t N) {

	PlanC myPlan = create_brugnano(N);
	Vector v(N, false);
	for (size_t i = 0; i < N; ++i) {
		v[i] = 1;
	}
	// cout << "starting to integrate\n";
	Vector r = myPlan.integrate(v);
//	cout << "r = " << r << "\n";
	cout << "starting to simple integrate\n";
	cout << "simple difference = " << norm(r - myPlan.integrate_simple(v, 1E-5))
			<< "\n";

}

void test_solve(size_t N) {
	PlanC myPlan = create_brugnano(N);

	Vector y(N);
	Vector r(N);
	y[1] = 1;
	y[2] = 1;

	//Vector zero(n);
	//Vector r = myPlan.findRoot();
	//cout << "Fix Point:\n\n";
	// cout << "residue = " << myPlan.eval(r);
	// cout << "root = " << r;

	cout << "Solve:\n\n";
	size_t cnt;
	r = myPlan.solve(y, cnt, 1E-9, true);
	cout << "Norm " << norm(y - myPlan.eval(r)) << "\n";
}

void test_jac(size_t N) {
	PlanC myPlan = create_brugnano(N);

	Vector v(N);
	for (size_t i = 0; i < N; ++i) {
		v[i] = i % 2 == 0 ? 0.001 : -0.001;
	}
	v[2] = 0.01;
	Vector x(N);
	Matrix genJac = myPlan.gen_jac(x, v);
	Matrix jac = myPlan.jac(x + v);
	cout << "genJac = " << genJac;
	cout << "jac = " << jac;
	cout << "genJac - jac" << genJac - jac;
	//TODO something wrong here.
}

PlanC create_rolling() {
	int n = 2;
	int m = 2;
	int s = 2;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	c[0] = -1;
	c[1] = 1;

	Z[0][0] = 1;
	Z[1][0] = 1;

	J[0][1] = 1;
	J[1][0] = -1;

	Y[1][0] = -0.5;
	Y[1][1] = 0.5;

	Vector xo(2);
	return PlanC(c, b, Z, L, J, Y, xo);
}

PlanC create_harmonic() {
	int n = 2;
	int m = 2;
	int s = 0;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	J[0][1] = 1;
	J[1][0] = -1;

	Vector xo(2);
	return PlanC(c, b, Z, L, J, Y, xo);
}

PlanC create_chua() {
	int n = 3;
	int m = 3;
	int s = 2;

	//TODO constants
	double m1 = -0.714;
	double m0 = -1.143;
	double alpha = 15.6;
	double beta = 28;

	Vector c(s);
	Vector b(m);

	Matrix Z(s, n);
	Matrix L(s, s);
	Matrix J(m, n);
	Matrix Y(m, s);

	c[0] = 1;
	c[1] = -1;

	Z[0][0] = 1;
	Z[1][0] = 1;

	J[0][0] = -(m1 + 1) * alpha;
	J[0][1] = alpha;
	J[1][0] = 1;
	J[1][1] = -1;
	J[1][2] = 1;
	J[2][1] = -beta;

	Y[0][0] = -alpha / 2 * (m0 - m1);
	Y[0][1] = alpha / 2 * (m0 - m1);

	Vector xo(n);
	return PlanC(c, b, Z, L, J, Y, xo);
}

Matrix rolling_exact(double h, double t_end, bool verbose) {
	size_t N = ceil(t_end / h);
	Matrix x(N, 2, false);

	size_t idx = 0;
	double t = 0;
	double val = 0;
	double grad = 0;

	if (verbose) {
		cout << "t x1 x2\n";
	}

	while (idx < N) {
		t = idx * h;
		while (t >= 2 * M_PI + 4) { //shift interval
			t -= 2 * M_PI + 4;
		}
		assert(0 <= t && t < 2 * M_PI + 4);
		if (t < M_PI) {
			val = 1 + sin(t);
			grad = cos(t);
		} else if (t < M_PI + 2) {
			val = 1 - (t - M_PI);
			grad = -1;
		} else if (t < 2 * M_PI + 2) {
			val = -1 - sin(2 - t);
			grad = cos(2 - t);
		} else {
			val = t - 3 - 2 * M_PI;
			grad = 1;
		}

		if (verbose) {
			cout << idx * h << " " << val << " " << grad << "\n";
		}

		x[idx][0] = val;
		x[idx][1] = grad;
		++idx;
	}

	return x;

}

void test_odes() {
//	PlanC rolling = create_rolling();
//	PlanC rolling = create_harmonic();
	PlanC chua = create_chua();
	Vector x0(3);
	x0[0] = 0.7;
	double t_end = 10;
	Matrix sol_plan = chua.solve_ode(pow(2, 12), x0, 0.0, t_end, true, 1E-8);
//	Matrix sol_plan = chua.solve_ode_simple(pow(2, 12), x0, 0.0, t_end, true);
//	cout << sol_plan;
//	Matrix sol_simple = chua.solve_ode_simple(h, x0, t_end, true);
}

void test_integrate_rolling() {
	double h = 1E-2;
	PlanC rolling = create_rolling();
	Vector xc(2, false);
	xc[0] = 1;
	xc[1] = 1;
	Vector xo = xc + h / 2 * rolling.eval(xc);
	Vector v = 2 * (xo - xc);
	Vector rp = rolling.integrate(v);
	Vector rm = rolling.integrate(-v);
	Vector r = h * (rp + rm - rolling.eval(xo));
	cout << "rp = " << rp << endl;
	cout << "rm = " << rm << endl;
	cout << "r = " << r << endl;
}

void test_integrate_harmonic() {
	PlanC harm = create_harmonic();
	Vector v(2);
	v[0] = 1;
	v[1] = 1;

	cout << harm.integrate(v) << "\n";
	cout << harm.integrate(-v) << "\n";
	cout << harm.integrate(v) + harm.integrate(-v) << "\n";
}

void test_simple() {
	double h = 1E-2;
	PlanC rolling = create_rolling();
	Vector x0(2);
	x0[0] = 1;
	x0[1] = 1;
	double t_end = 20;

	Matrix sol_simple = rolling.solve_ode_midpoint_explicit(h, x0, t_end,
			false);
	Matrix sol_exact = rolling_exact(h, t_end, false);

	Matrix diff = sol_exact - sol_simple;

	cout << "t error\n";
//	size_t n = diff.getRows()/1000;
	size_t n = 1;
	for (size_t i = 0; i < diff.getRows(); i += n) {
		cout << i * h << " " << fabs(diff[i][0]) << "\n";
	}
}

void convergence_rolling(double h_max, double h_min, size_t N, double TOL) {
	PlanC rolling = create_rolling();
	Vector x0(2);
	x0[0] = 1;
	x0[1] = 1;

	double t_end = 20;

	Vector E_plan(N);
	Vector E_simple(N);
	double dH = (h_max - h_min) / N;
	cout << "h err_plan err_simple\n";
	for (size_t n = 0; n < N; ++n) {
		double h = pow(10, h_min + n * dH);

		Matrix sol_exact = rolling_exact(h, t_end, false);

		Matrix err_plan = sol_exact
				- rolling.solve_ode(pow(10, h_min + n), x0, 0, t_end, false,
						TOL);
		Matrix err_simple = sol_exact
				- rolling.solve_ode_midpoint_explicit(h, x0, t_end, false);

		for (size_t i = 0; i < err_plan.getRows(); ++i) {
			if (fabs(err_plan[i][0]) > E_plan[n])
				E_plan[n] = fabs(err_plan[i][0]);
			if (fabs(err_plan[i][1]) > E_plan[n])
				E_plan[n] = fabs(err_plan[i][1]);

			if (fabs(err_simple[i][0]) > E_simple[n])
				E_simple[n] = fabs(err_simple[i][0]);
			if (fabs(err_simple[i][1]) > E_simple[n])
				E_simple[n] = fabs(err_simple[i][1]);
		}

		cout << h << " " << E_plan[n] << " " << E_simple[n] << "\n";
		cout.flush();
	}
}

void convergence(PlanC p, Vector x0, double t_end, size_t s, size_t N,
		double TOL) {
	//calculate convergence for 2^s to 2^s+N and compare to 2^s+N+1
	Vector E_plan(N);
	Vector E_mid_exp(N);
	Vector E_mid_impl(N);

	Vector T_plan(N);
	Vector T_mid_exp(N);
	Vector T_mid_impl(N);

	size_t n_ex = s + N + 1;

	cout << "# plot from 2^" << s << " to 2^" << s + N << " against 2^"
			<< s + N + 1 << endl;

	Matrix sol_exact = p.solve_ode(pow(2, n_ex), x0, 0, t_end, false, TOL);

	size_t i, j, i_ex;
	clock_t t;
//	cout << "rows exact = " << sol_exact.getRows() << endl;

	cout << "n err_plan err_exp err_impl t_plan t_exp t_impl\n";
	for (size_t n = 0; n < N; ++n) {

		cout << pow(2, s + n) << " ";

		t = clock();
		Matrix plan = p.solve_ode(pow(2, s + n), x0, 0, t_end, false, TOL);
		T_plan[n] = ((float) (clock() - t)) / CLOCKS_PER_SEC;

		t = clock();
		Matrix mid_exp = p.solve_ode_midpoint_explicit(pow(2, s + n), x0, 0,
				t_end, false);
		T_mid_exp[n] = ((float) (clock() - t)) / CLOCKS_PER_SEC;

		t = clock();
		Matrix mid_impl = p.solve_ode_midpoint_implicit(pow(2, s + n), x0, 0,
				t_end, false, TOL);
		T_mid_impl[n] = ((float) (clock() - t)) / CLOCKS_PER_SEC;

		//calculate error
		for (i = 0; i < plan.getRows(); ++i) {

			i_ex = round(i * pow(2, n_ex - s - n));
			for (j = 0; j < plan.getColumns(); ++j) {
				E_plan[n] += pow(
						(plan[i][j] - sol_exact[i_ex][j]) / plan.getColumns(),
						2.0);
				E_mid_exp[n] += pow(
						(mid_exp[i][j] - sol_exact[i_ex][j])
								/ mid_exp.getColumns(), 2.0);
				E_mid_impl[n] += pow(
						(mid_impl[i][j] - sol_exact[i_ex][j])
								/ mid_impl.getColumns(), 2.0);
			}
		}
		E_plan[n] = sqrt(E_plan[n]) / pow(2, (s + n) / 2);
		E_mid_exp[n] = sqrt(E_mid_exp[n]) / pow(2, (s + n) / 2);
		E_mid_impl[n] = sqrt(E_mid_impl[n]) / pow(2, (s + n) / 2);
		cout << E_plan[n] << " " << E_mid_exp[n] << " " << E_mid_impl[n] << " ";
		cout << T_plan[n] << " " << T_mid_exp[n] << " " << T_mid_impl[n]
				<< endl;
	}

}

void convergence_general(PlanC p, Vector x0, double t_0, double t_end,
		Vector NN, double N_exact, double TOL) {

	size_t N = NN.length();
	Vector E_plan(N);
	Vector E_explicit(N);

	cout << "# plot from " << NN[0] << " to " << NN[N - 1] << " in " << N
			<< " steps against " << N_exact << endl;

	Matrix sol_exact = p.solve_ode(N_exact, x0, t_0, t_end, false, TOL);

	size_t i, j, i_ex;
	double frac_part, int_part, tmp;

	cout << "n err_plan err_simple\n";
	for (size_t n = 0; n < N; ++n) {

//		cout << round(NN[n]) << " ";

		Matrix plan = p.solve_ode(round(NN[n]), x0, t_0, t_end, false, TOL);

		E_plan[n] = error(sol_exact, plan);
//		cout << E_plan[n] << " ";

		Matrix sol_explicit = p.solve_ode_midpoint_explicit(round(NN[n]), x0,
				t_0, t_end, false);

		E_explicit[n] = error(sol_exact, sol_explicit);

//		cout << E_explicit[n] << endl;
	}

}

Vector logSpace(double base, double min, size_t num) {
	size_t i;

	Vector NN(num, false);
	for (i = 0; i < num; ++i) {
		NN[i] = round(pow(base, min + i));
	}

	return NN;
}

Vector linSpace(double min, double max, size_t num) {
	size_t i;
	double h = (max - min) / (num - 1);

	Vector NN(num, false);
	for (i = 0; i < num; ++i) {
		NN[i] = min + i * h;
	}

	return NN;
}

Vector error_over_time(PlanC p, Vector x0, double t_0, double t_end,
		double N_er, double N_ex, double TOL) {
	size_t sample_size = 500;

	cout << "# error over time" << endl;
	cout << "# of " << N_er << " against " << N_ex << endl;

	Vector it_plan(N_er), it_impl(N_er);

	Matrix sol_ex = p.solve_ode(N_ex, x0, t_0, t_end, false, TOL);
	Matrix sol_plan = p.solve_ode(N_er, x0, t_0, t_end, false, TOL, it_plan);
	Matrix sol_impl = p.solve_ode_midpoint_implicit(N_er, x0, t_0, t_end, false,
			TOL, it_impl);
	Matrix sol_expl = p.solve_ode_midpoint_explicit(N_er, x0, t_0, t_end,
			false);

	size_t i, j;
	double i_ex;

	double h = (t_end - t_0) / N_er;

	Vector err_plan(N_er);
	Vector err_impl(N_er);
	Vector err_exp(N_er);

	cout << "t err_plan err_exp err_impl it_plan it_impl" << endl;
	for (i = 0; i < N_er; ++i) {

		i_ex = round(N_ex / N_er * i);
		for (j = 0; j < sol_plan.getColumns(); ++j) {
			err_plan[i] += pow(sol_plan[i][j] - sol_ex[i_ex][j], 2);
			err_impl[i] += pow(sol_impl[i][j] - sol_ex[i_ex][j], 2);
			err_exp[i] += pow(sol_expl[i][j] - sol_ex[i_ex][j], 2);
		}
		err_plan[i] = sqrt(err_plan[i]);
		err_impl[i] = sqrt(err_impl[i]);
		err_exp[i] = sqrt(err_exp[i]);

//		cout << t << " " << err_plan[i] << " "<< err_simple[i] <<endl;
//		t += h;
	}

	size_t h_idx = N_er / sample_size;
	for (i = 0; i < sample_size; ++i) {
		size_t idx = i * h_idx;
		cout << idx * h << " " << err_plan[idx] << " " << err_exp[idx] << " "
				<< err_impl[idx] << " ";
		cout << it_plan[idx] << " " << it_impl[idx] << endl;
	}

	return err_plan;
}

void test_factorize() {
//	PlanC plan = create_chua();
	PlanC plan = create_brugnano(3);
	plan.factorize();
//	cout << chua;
}

void rolling(short tag, double* px, double* py) {
	enableMinMaxUsingAbs();
	trace_on(tag);
	adouble *x, *y;
	size_t n = 2;
	size_t m = 2;

	x = new adouble[n];
	y = new adouble[m];

	for (size_t i = 0; i < n; i++) {
		x[i] <<= px[i];
	}

	y[0] = x[1];
	y[1] = -x[0] - 0.5 * fabs(x[0] - 1) + 0.5 * fabs(x[0] + 1);

	for (size_t j = 0; j < m; j++) {
		y[j] >>= py[j];
	}
	delete[] y;
	delete[] x;

	trace_off(tag);
}

void tischendorf(short tag, double* px, double* py) {
	enableMinMaxUsingAbs();
	trace_on(tag);
	adouble *x, *y;
	size_t n = 3;
	size_t m = 3;

	x = new adouble[n];
	y = new adouble[m];

	for (size_t i = 0; i < n; i++) {
		x[i] <<= px[i];
	}

	double L = 1E-6;
	double C = 1E-13;
	double om = 3E9;
	double alpha = 2;
	double beta = 1E-5;

	adouble g1 = (C * x[2] + fabs(C * x[2])) / (2 * alpha)
			+ (C * x[2] - fabs(C * x[2])) / (2 * beta);
	y[0] = 1;
	y[1] = x[2];
	y[2] = -1 / (L * C) * (x[1] - C * sin(om * x[0]) + g1);

	for (size_t j = 0; j < m; j++) {
		y[j] >>= py[j];
	}
	delete[] y;
	delete[] x;

	trace_off(tag);
}

void pretty(short tag, double* px, double* py) {
	enableMinMaxUsingAbs();
	trace_on(tag);
	adouble *x, *y;
	size_t n = 1;
	size_t m = 1;

	x = new adouble[n];
	y = new adouble[m];

	for (size_t i = 0; i < n; i++) {
		x[i] <<= px[i];
	}

	y[0] = -fabs(x[0]) + 1;
//	y[0] = fmax(sin(4 * x[0]), -x[0] * x[0] + 0.6);
//	y[0] = fmax(fmin(exp(x[0]), -(2 * x[0]) * (2 * x[0]) + 2),
//			atan(4 * x[0]) + 1);
	//y[0] = (0.5*x[0] -1) * fabs(x[0]);
//	y[0] = fmax(-6.0 / 10 * (x[0] + 2) * (x[0] + 2),
//			3 * (x[0] + sin(11.0 / 10 * x[0])));
//	y[0] = fmax(sin(x[0]),-x[0]*x[0] +2);

	//y[0] = fabs(sin(x[0]));
//	y[0] = fabs(exp(x[0]) - 3*x[0]);
//	y[0] = sin(fabs(x[0]));

	for (size_t j = 0; j < m; j++) {
		y[j] >>= py[j];
	}
	delete[] y;
	delete[] x;

	trace_off(tag);
}

void func_3D(short tag, double* px, double* py) {
	enableMinMaxUsingAbs();
	trace_on(tag);
	adouble *x, *y;
	size_t n = 2;
	size_t m = 1;

	x = new adouble[n];
	y = new adouble[m];

	for (size_t i = 0; i < n; i++) {
		x[i] <<= px[i];
	}

	y[0] = sin(fabs(x[0] - exp(fabs(x[1] - x[0]))));

	for (size_t j = 0; j < m; j++) {
		y[j] >>= py[j];
	}
	delete[] y;
	delete[] x;

	trace_off(tag);
}

void test_adolc() {
//	Vector x0(3);
//	PlanC t(3, 3, x0, tischendorf);

	Vector x0(1);
	x0[0] = 1;
	PlanC t(1, 1, x0, pretty);
	//cout << "t = " << t;
	t.plot(1000, 2, 0, 0);
}

void create_animation() {
	size_t nbr_pics = 50;
	size_t x_ind = 0; //idx of x value
	size_t y_ind = 0; //idx of y value
	size_t N = 300; //number of samples

	size_t n = 1; //dimension of x
	size_t m = 1; //dimension of y

	Vector x0(n); //first base point for linearization
	x0[x_ind] = -1.5;
	//x0[0] = 1;

	double x_length = 2.5;
	double h_plot = x_length / N;
	double h_anim = x_length / nbr_pics;

	cout << "# x0 = " << x0[x_ind] << " h_anim = " << h_anim << endl;

	Matrix val(N, nbr_pics + 2); //all points to plot
	Vector py(m, false);

	Vector x0_cur = x0;
	for (size_t i = 0; i < nbr_pics; i++) {
		PlanC t(n, m, x0_cur, pretty);

		Vector x = x0;
		for (size_t j = 0; j < N; j++) {
			py = t.eval(x);
			x[x_ind] += h_plot;
			val[j][i] = py[y_ind];
		}
		x0_cur[x_ind] += h_anim;
	}

	//the real values
	Vector x = x0;
	Vector y(m, false);
	PlanC t(n, m, x0, pretty);

	for (size_t j = 0; j < N; j++) {
		y = t.eval_F(x);
		val[j][nbr_pics] = y[y_ind];
		val[j][nbr_pics + 1] = x[x_ind];
		x[x_ind] += h_plot;
	}
	for (size_t i = 0; i < nbr_pics; i++) {
		cout << "py" << i << " ";
	}
	cout << "y x" << endl;
	cout << val;
}

void create_animation_3D() {
	//dimensions must be this way
	size_t n = 2; //dimension of x
	size_t m = 1; //dimension of y
	size_t N = 50; //number both directions. square for dof
	size_t N_pics = 100;

	Vector x_start(n);	//lower left corner of plot
	double h = 0.04;		//discretization for plot

	Vector* x0 = new Vector[N_pics];
	//set base point path here
	for (size_t i = 0; i < N_pics; ++i) {
		x0[i] = Vector(n, false);
		x0[i][0] = 1 + sin(i * 2 * M_PI / N_pics);
		x0[i][1] = 1 + cos(i * 2 * M_PI / N_pics);
		//cout << x0[i] << "\targ= "<< 2*M_PI/N_pics << "\tsin= "<< sin(2*M_PI/N_pics) << endl;
	}

	Matrix val(N * N, N_pics + 3); //all points to plot
	Matrix meta(N_pics, 3);
	Vector x(n);
	Vector y(m);

	//linearization values by picture
	for (size_t pic_ind = 0; pic_ind < N_pics; pic_ind++) {
		PlanC t(n, m, x0[pic_ind], func_3D);
		meta[pic_ind][0] = x0[pic_ind][0];
		meta[pic_ind][1] = x0[pic_ind][1];
		meta[pic_ind][2] = t.eval_F(x0[pic_ind])[0];

		for (size_t x_ind = 0; x_ind < N; x_ind++) {
			for (size_t y_ind = 0; y_ind < N; y_ind++) {
				x = x_start;
				x[0] += x_ind * h;
				x[1] += y_ind * h;

				y = t.eval(x);
				val[x_ind * N + y_ind][pic_ind] = y[0];
			}
		}
	}

	PlanC t(n, m, x0[0], func_3D);
	//actual values
	for (size_t x_ind = 0; x_ind < N; x_ind++) {
		for (size_t y_ind = 0; y_ind < N; y_ind++) {
			x = x_start;
			x[0] += x_ind * h;
			x[1] += y_ind * h;

			y = t.eval_F(x);
			val[x_ind * N + y_ind][N_pics] = y[0];
			val[x_ind * N + y_ind][N_pics + 1] = x[0];
			val[x_ind * N + y_ind][N_pics + 2] = x[1];
		}
	}

	//output
	cout << "#meta data: base point and value there" << endl;
	cout << "x0 x1 y" << endl;
	cout << meta << endl;

	cout << "# x0 = (0,0) \t h=" << h << endl;
	for (size_t i = 0; i < N_pics; i++) {
		cout << "py" << i << " ";
	}
	cout << "y x0 x1" << endl;
	cout << val;
}

void arnold(short tag, double* px, double* py) {
	enableMinMaxUsingAbs();

	trace_on(tag);
	adouble *x, *y;
	size_t n = 3;
	size_t m = 3;

	x = new adouble[n];
	y = new adouble[m];

	for (size_t i = 0; i < n; i++) {
		x[i] <<= px[i];
	}

	double gamma = 1;
	double eta = 1;
	double eps = 1E-2;

	adouble sgn = fmax(-1, fmin(1 / eps * x[1], 1));

	y[0] = 1;
	y[1] = x[2];
	y[2] = gamma * sin(eta * x[0]) - sgn - x[1];

	for (size_t j = 0; j < m; j++) {
		y[j] >>= py[j];
	}
	delete[] y;
	delete[] x;

	trace_off(tag);
}

void convergence_chua() {
	PlanC p = create_chua();
	Vector x0(3);
	x0[0] = 0.7;
	double t_end = 10;
//	Vector NN = linSpace(pow(2,10), pow(2,17), 15);
//	Vector NN = logSpace(2, 10, 1);
//	convergence_general(p, x0, 0, t_end, NN, pow(2, 11), 1E-13);
//	cout << endl << endl;
	convergence(p, x0, t_end, 10, 5, 1E-13);
//	error_over_time(p,x0,0,t_end,1E3,1E4,1E-10);

}

void convergence_arnold() {

	Vector x0(3);
	x0[1] = 1;
	PlanC p(3, 3, x0, arnold);
	double t_end = 10;
	//p.solveODEnew(1000, x0, 0, 10, true, 1E-6);
//	p.solve_ode_simple(pow(2,14),x0,0,10,true);
//	convergence(p, x0, 10, 9, 9, 1E-9);
//	Vector NN = linSpace(1E4, 1E5, 20);
//	convergence_general(p, x0, 0, t_end, NN, 1E6, 1E-10);
	convergence(p, x0, t_end, 9, 10, 1E-10);
//	error_over_time(p, x0, 0, t_end, 1E3, 1E4, 1E-10);
}

void convergence_tischendorf() {
	size_t n = 3;
	Vector x0(n);

	double t_end = 1.5E-8;
	PlanC p(3, 3, x0, tischendorf);

//	error_over_time(p,x0,0,t_end,1E4,1E5,1E-10);

//	Vector NN = linSpace(1E2, 1E6, 30);
//	convergence_general(p, x0, 0, t_end, NN, 1E7, 1E-10);
//	p.solve_ode(pow(2, 25), x0, 0, t_end, true, 1E-10);
	convergence(p, x0, t_end, 10, 10, 1E-12);
//	p.solveODEnew(pow(2,16),x0,0,t_end,true,1E-10);

}

void exp_eot() {
	cout << "# This is EOT" << endl;

//	cout << "# CHUA" << endl;
//	PlanC p = create_chua();
//	Vector x0(3);
//	x0[0] = 0.7;
//	double t_end = 10;
//	error_over_time(p, x0, 0, t_end, 1E4, 1E5, 1E-10);
//	cout << endl << endl;

//	cout << "# ARNOLD" << endl;
//	Vector x0(3);
//	x0[1] = 1;
//	PlanC p(3, 3, x0, arnold);
//	double t_end = 10;
//	error_over_time(p, x0, 0, t_end, 1E4, 1E5, 1E-10);
//	cout << endl << endl;

	cout << "# TISCHENDORF" << endl;
	Vector x0(3);
	double t_end = 1.5E-8;
	PlanC p(3, 3, x0, tischendorf);
	error_over_time(p, x0, 0, t_end, 1E4, 1E5, 1E-10);

}

int main(void) {

//	convergence_tischendorf();
//	convergence_arnold();
	cout << "#chua" << endl;
	convergence_chua();
//	cout << endl << endl;

//	exp_eot();

//	time_experiment();
//cout << "#arnold" << endl;
//convergence_arnold();
//	cout << endl << endl;

//	cout << "#tischendorf" << endl;
//	convergence_tischendorf();
}
