#include <assert.h>
#include <iostream>
#include <math.h>
#include <string.h>
#include "mvlib.hpp"

using namespace std;

//see memory movement
//void *Malloc(size_t size) {
//	cout << "Malloc " << size << '\n';
//	return malloc(size);
//}
//void *Calloc(size_t num, size_t size) {
//	cout << "Calloc " << num * size << '\n';
//	return calloc(num, size);
//}
//
//void Free(void* ptr) {
//	cout << "Free\n";
//	free(ptr);
//}
//#define malloc Malloc
//#define calloc Calloc
//#define free Free

/***************** Vector *****************/
Vector::Vector() :
		data_(nullptr), length_(0) {
}
Vector::Vector(size_t length, double* x_in) :
		length_(length) {
	data_ = (double *) malloc(length_ * sizeof(*data_));
	memcpy(data_, x_in, length_ * sizeof(*data_));
}
Vector::Vector(size_t length, bool initialized) :
		length_(length) {
	if (initialized)
		data_ = (double *) calloc(length_, sizeof(*data_)); //0-initialisiert
	else
		data_ = (double *) malloc(length_ * sizeof(*data_));
	assert(data_);
}
Vector::Vector(const Vector &other) :
		length_(other.length_) {
	data_ = (double *) malloc(length_ * sizeof(*data_));
	memcpy(data_, other.data_, length_ * sizeof(*data_));
}
Vector::Vector(const Row &other) :
		length_(other.length_) {
	data_ = (double *) malloc(length_ * sizeof(*data_));
	memcpy(data_, other.data_, length_ * sizeof(*data_));
}
Vector::Vector(Vector &&other) :
		data_(nullptr), length_(other.length_) {
	swap(data_, other.data_);
}
Vector::~Vector() {
	free(data_);
}
Vector Vector::abs() const {
	Vector result(length_, false);
	for (size_t i = 0; i < length_; ++i) {
		result[i] = fabs(data_[i]);
	}
	return result;
}
size_t Vector::length() const {
	return length_;
}
Vector &Vector::operator =(const Vector &other) {
	if (this != &other) {
		assert(length_ == other.length_);
		memcpy(data_, other.data_, length_ * sizeof(*data_));
	}
	return *this;
}
Vector &Vector::operator =(Vector &&other) {
	if (this != &other) {
		length_ = other.length_;
		swap(data_, other.data_);
	}
	return *this;
}
double &Vector::operator [](size_t index) {
	assert(index < length_);
	return data_[index];
}
double const Vector::operator [](size_t index) const {
	assert(index < length_);
	return data_[index];
}
Vector const Vector::operator -() const {
	Vector result(length_, false);
	for (size_t i = 0; i < length_; ++i) {
		result[i] = -data_[i];
	}
	return result;
}
Vector &Vector::operator *=(const double &d) {
	for (size_t i = 0; i < length_; ++i) {
		data_[i] *= d;
	}
	return *this;
}
double err(const Vector &v, const Vector &w) {
	assert(v.length_ == w.length_);
	double r = 0;
	for (size_t i = 0; i < v.length_; ++i) {
		r += fabs(v[i] - w[i]);
	}
	return r;
}
void Vector::insertBlock(const Vector &v, size_t idx) {
	assert(idx + v.length_ <= length_);
	memcpy(data_ + idx, v.data_, v.length_ * sizeof(*data_));
}

/***************** Row *****************/
Row::Row(double* data, size_t length) :
		data_(data), length_(length) {
}
;
size_t Row::length() const {
	return length_;
}
double &Row::operator [](size_t index) {
	assert(index < length_);
	return data_[index];
}
double const &Row::operator [](size_t index) const {
	assert(index < length_);
	return data_[index];
}
Row &Row::operator *=(const double &d) {
	for (size_t i = 0; i < length_; ++i) {
		data_[i] *= d;
	}
	return *this;
}
Row &Row::operator =(const Vector &other) {
	assert(length_ == other.length_);
	memcpy(data_, other.data_, length_ * sizeof(*data_));
	return *this;
}

/***************** Matrix *****************/
Matrix::Matrix() :
		data_(nullptr), rows_(0), columns_(0) {
}
;
Matrix::Matrix(size_t rows, size_t columns, bool initialized) :
		rows_(rows), columns_(columns) {
	if (initialized)
		data_ = (double*) calloc(rows * columns, sizeof(*data_));
	else
		data_ = (double*) malloc(rows * columns * sizeof(*data_));
	assert(data_);
}
Matrix::Matrix(Matrix &&other) :
		data_(nullptr), rows_(other.rows_), columns_(other.columns_) {
	swap(data_, other.data_);
}
Matrix::Matrix(const Matrix &other) :
		rows_(other.rows_), columns_(other.columns_) {
	data_ = (double *) malloc(rows_ * columns_ * sizeof(*data_));
	memcpy(data_, other.data_, rows_ * columns_ * sizeof(*data_));
}
Matrix::~Matrix() {
	free(data_);
}
void Matrix::insertRow(size_t row, double* in) {
	memcpy(data_ + row * columns_, in, columns_ * sizeof(*data_));
}
void Matrix::insertBlock(size_t row, size_t column, Matrix &block) {
	assert(row + block.rows_ <= rows_ && column + block.columns_ <= columns_);
	for (size_t i = 0; i < block.rows_; ++i) {
		memcpy(data_ + (i + row) * columns_ + column,
				block.data_ + i * block.columns_,
				block.columns_ * sizeof(*data_));
	}
}
void Matrix::insertBlock(size_t r_out, size_t c_out, Matrix &m, size_t r_in,
		size_t c_in, size_t r_n, size_t c_n) {
	assert(r_out + r_n <= rows_);
	assert(c_out + c_n <= columns_);
	assert(r_in + r_n <= m.rows_);
	assert(c_in + c_n <= m.columns_);
	for (size_t i = 0; i < r_n; ++i) {
		memcpy(data_ + (i + r_out) * columns_ + c_out,
				m.data_ + i * m.columns_ + c_in, c_n * sizeof(*data_));
	}
}
Row Matrix::operator [](size_t row) {
	assert(row < rows_);
	return Row(data_ + columns_ * row, columns_);
}
Row const Matrix::operator [](size_t row) const {
	assert(row < rows_);
	return Row(data_ + columns_ * row, columns_);
}
Matrix& Matrix::operator =(const Matrix &other) {
	if (this != &other) {
		assert(rows_ == other.rows_ && columns_ == other.columns_);
		memcpy(data_, other.data_, rows_ * columns_ * sizeof(*data_));
	}
	return *this;
}
Matrix& Matrix::operator =(Matrix &&other) {
	rows_ = other.rows_;
	columns_ = other.columns_;
	swap(data_, other.data_);
	return *this;
}
const Matrix Matrix::operator -() const {
	Matrix result(rows_, columns_, false);
	for (size_t i = 0; i < rows_ * columns_; ++i) {
		result.data_[i] = -data_[i];
	}
	return result;
}
Matrix& Matrix::operator *=(const double &d) {
	for (size_t i = 0; i < rows_ * columns_; ++i) {
		data_[i] *= d;
	}
	return *this;
}
size_t Matrix::getRows() const {
	return rows_;
}
size_t Matrix::getColumns() const {
	return columns_;
}

/***************** SubMatrix *****************/
SubMatrix::SubMatrix(Matrix &m, size_t row_offset, size_t column_offset,
		size_t rows, size_t columns) :
		rows_(rows), columns_(columns), row_offset_(row_offset), column_offset_(
				column_offset), matrix(m) {
	assert(column_offset_ + columns <= m.columns_);
	assert(row_offset_ + rows <= m.rows_);
}

SubMatrix& SubMatrix::operator =(const Matrix &other) {
	assert(rows_ == other.rows_ && columns_ == other.columns_);
	for (size_t r = 0; r < rows_; ++r) {
		for (size_t c = 0; c < columns_; ++c) {
			matrix.data_[(row_offset_ + r) * matrix.columns_ + column_offset_
					+ c] = other[r][c];
		}
	}
	return *this;
}

SubMatrix &SubMatrix::operator =(const SubMatrix &other) {
	rows_ = other.rows_;
	row_offset_ = other.row_offset_;
	columns_ = other.columns_;
	column_offset_ = other.column_offset_;
	matrix = other.matrix;
	return *this;
}

Row SubMatrix::operator [](size_t row) {
	return Row(
			const_cast<double*>(matrix.data_)
					+ matrix.columns_ * (row + row_offset_) + column_offset_,
			columns_);
}
Row const SubMatrix::operator [](size_t row) const {
	return Row(
			const_cast<double*>(matrix.data_)
					+ matrix.columns_ * (row + row_offset_) + column_offset_,
			columns_);
}
size_t SubMatrix::getRows() const {
	return rows_;
}
size_t SubMatrix::getColumns() const {
	return columns_;
}

ostream& operator <<(ostream &os, const Vector &v) {
	for (size_t i = 0; i < v.length() - 1; ++i)
		os << v[i] << ' ';
	os << v[v.length() - 1];
	return os;
}
ostream& operator <<(ostream &os, const Matrix &m) {
	os << "[" << endl;
	for (size_t i = 0; i < m.getRows(); ++i) {
		for (size_t j = 0; j < m.getColumns(); ++j)
			os << m[i][j] << ' ';
		os << endl;
	}
	os << "]" << endl;
	return os;
}
ostream& operator <<(ostream &os, const SubMatrix &m) {
	os << "[" << endl;
	for (size_t i = 0; i < m.getRows(); ++i) {
		for (size_t j = 0; j < m.getColumns(); ++j)
			os << m[i][j] << ' ';
		os << endl;
	}
	os << "]" << endl;
	return os;
}

Matrix spline(Vector x_g, Matrix y_g, Vector x) {
	size_t i, j;
	double p, sig;
	size_t n = x_g.length();
	size_t m = y_g.getColumns();

	//building spline
	Vector u(n - 1); //temp
	Matrix y2(n, m); //second derivative of y

	for (j = 0; j < m; j++) {
		y2[0][j] = 0; //natural spline
		u[0] = 0;
		for (i = 1; i < n - 1; i++) {
			sig = (x_g[i] - x_g[i - 1]) / (x_g[i + 1] - x_g[i - 1]);
			p = sig * y2[i - 1][j] + 2;
			y2[i][j] = (sig - 1) / p;
			u[i] = (y_g[i + 1][j] - y_g[i][j]) / (x_g[i + 1] - x_g[i])
					- (y_g[i][j] - y_g[i - 1][j]) / (x_g[i] - x_g[i - 1]);
			u[i] = (6 * u[i] / (x_g[i + 1] - x_g[i - 1]) - sig * u[i - 1]) / p;
		}

		y2[n - 1][j] = 0; //natural spline
		for (int i = n - 2; i >= 0; i--) {
			y2[i][j] = y2[i][j] * y2[i + 1][j] + u[i];
		}
	}
	//evaluating spline
	Matrix y_out(x.length(), m);

	double h = x_g[1] - x_g[0];
	double h_i = x[1] - x[0];
	double a, b;
	int khi = 0;

	for (j = 0; j < m; j++) {
		y_out[0][j] = y_g[0][j];
		for (i = 1; i < x.length(); i++) {
			while (h_i * i >= x_g[khi]) {
				khi++;
			}
			a = (x_g[khi] - x[i]) / h;
			b = (x[i] - x_g[khi - 1]) / h;
			y_out[i][j] = a * y_g[khi - 1][j] + b * y_g[khi][j]
					+ ((a * a * a - a) * y2[khi - 1][j]
							+ (b * b * b - b) * y2[khi][j]) * (h * h) / 6.0;
		}
	}
	return y_out;
}

double error(Matrix x_big, Matrix x_small) {
	size_t n_big = x_big.getRows();
	size_t n_small = x_small.getRows();
	size_t m = x_small.getColumns();

	Vector t_big(n_big);
	double h_big = 1.0 / n_big;
	for (size_t i = 1; i < n_big; i++) {
		t_big[i] = i * h_big;
	}

	Vector t_small(n_small);
	double h_small = 1.0 / n_small;
	for (size_t i = 1; i < n_small; i++) {
		t_small[i] = i * h_small;
	}

	Matrix diff = spline(t_big, x_big, t_small) - x_small;

	cout << diff << endl;

	double err = 0;
	for (size_t i = 0; i < n_small; i++) {
		for (size_t j = 0; j < m; j++) {
			err += pow(diff[i][j] / m, 2);
		}
	}

	return sqrt(err) / n_small;
}
