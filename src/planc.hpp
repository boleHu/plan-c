#ifndef PLANC_H
#define PLANC_H

#include "mvlib.hpp"

//function handle of the form func(x,y) with f(x) = y
typedef void (*simple_function)(short tag, double* in_x, double* out_y);

// This class represents the piecewise linear function F(x) = y in the abs-normal form.

class PlanC {
private:
	const size_t MAX_IT = 200;

	size_t n; //dimension of x
	size_t m; //dimension of y
	size_t s; //dimension of z, number of switches
	bool factorized;

	Vector c;
	Vector b;
	Matrix Z;
	Matrix L;
	Matrix J;
	Matrix Y;
	Matrix Big;
	Vector idx;	//permutation for LUJ

	SubMatrix S; //upper right submatrix of Big
	SubMatrix LUJ; //lower left submatrix of Big

	simple_function F;
	short tag;
	Vector xo;
	Vector fxo;

	void updateLinearization(const Vector &in_x);

	Vector getCHat(const Vector &y);
	Vector calculate_z(const Vector &x);
	Vector calculate_x(Vector z, Vector y);
	Vector eval(Vector z, Vector x);

	int firstsign(const double &z, Row gzj);
public:
	PlanC(Vector &in_c, Vector &in_b, Matrix &in_Z, Matrix &in_L, Matrix &in_J, Matrix &in_Y, Vector &in_xo);
	PlanC(size_t n_in, size_t m_in, size_t s_in);
	PlanC(const PlanC &other);
	PlanC(size_t in_n, size_t in_m, const Vector &in_x, simple_function in_func,short tag = 0);

	~PlanC();

	//inserts row of the size n+s
	void insertRow(size_t row, double* in);

	//factorize Big
	void factorize();
	bool is_factorized();

	PlanC linear_combination(double alpha, double beta);

	//integrates the function along t*v with t in [0,0.5]
	Vector integrate(const Vector v);
	//integrates ignoring kinks
	Vector integrate_simple(Vector v, const double h);
	//evaluates the function at point x
	Vector eval(const Vector &x);
	//evaluates underlying function at point x
	Vector eval_F(const Vector &x);

	//solves F(x) = 0 with a simple fix point iteration
	Vector findRoot(double TOL);
	//solves F(x) = y with a newton method
	Vector solve(const Vector &y, size_t &cnt_it, double TOL,bool verbose=false);

	double crit_mult(const Vector &x, const Vector &dx, size_t crit_idx, const Vector &sig);

	// Vector newton(const Vector &y);
	Matrix gen_jac(const Vector &x, const Vector &v);
	Matrix jac(const Vector &x);

	Matrix solve_ode(const double N, const Vector &x0, const double t0,
			const double t_end, bool verbose, double TOL,Vector &cnt_its=__NULL_VECTOR__);

	Matrix solve_ode_midpoint_implicit(const double N_in, const Vector &x0, const double t0,
			const double t_end, bool verbose, double TOL,Vector &cnt_its=__NULL_VECTOR__);

	Matrix solve_ode_midpoint_explicit(double N, const Vector &x0, double t_0,
			double t_end, bool verbose = false);

	void plot(size_t N, double x_end);
	void plot(size_t N, double interval, size_t x_ind, size_t y_ind);

	friend ostream& operator <<(ostream &os, const PlanC &p);
};
#endif
