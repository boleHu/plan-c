#include "planc.hpp"
#include "mvlib.hpp"
#include <math.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <adolc/adolc.h>

using namespace std;
PlanC::PlanC(Vector &in_c, Vector &in_b, Matrix &in_Z, Matrix &in_L,
		Matrix &in_J, Matrix &in_Y, Vector &in_xo) :
		n(in_J.getColumns()), m(in_J.getRows()), s(in_L.getRows()), factorized(
				false), c(in_c), b(in_b), Z(in_Z), L(in_L), J(in_J), Y(in_Y), Big(
				m + s, n + s, false), idx(n, false), S(Big, 0, n, s, s), LUJ(
				Big, s, 0, m, n), F(nullptr), tag(0), xo(in_xo), fxo(eval(xo)) {
}

PlanC::PlanC(size_t n_in, size_t m_in, size_t s_in) :
		n(n_in), m(m_in), s(s_in), factorized(false), c(s), b(m), Z(s, n), L(s,
				s), J(m, n), Y(m, s), Big(m + s, n + s, false), idx(n, false), S(
				Big, 0, n, s, s), LUJ(Big, s, 0, m, n), F(nullptr), tag(0), xo(
				n, false), fxo(m, false) {
}

PlanC::~PlanC() {
}

PlanC::PlanC(const PlanC &other) :
		n(other.n), m(other.m), s(other.s), factorized(other.factorized), c(
				other.c), b(other.b), Z(other.Z), L(other.L), J(other.J), Y(
				other.Y), Big(other.Big), idx(other.idx), S(Big, 0, n, s, s), LUJ(
				Big, s, 0, m, n), F(other.F), tag(other.tag), xo(other.xo), fxo(
				other.fxo) {
}

PlanC::PlanC(size_t in_n, size_t in_m, const Vector &in_x,
		simple_function in_func, short in_tag) :
		PlanC(in_n, in_m, 0) {
	//TODO adolc constructor with less reallocation

	F = in_func;
	double* x = new double[n];
	double* y = new double[m];

	for (size_t i = 0; i < n; ++i) {
		x[i] = in_x[i];
	}

	tag = in_tag;
	in_func(tag, x, y);
	int keep = 1;
	s = get_num_switches(tag);

	Z = Matrix(s, n, false);
	L = Matrix(s, s, false);
	Y = Matrix(m, s, false);
	Big = Matrix(m + s, n + s, false);
	S = SubMatrix(Big, 0, n, s, s);
	LUJ = SubMatrix(Big, s, 0, m, n);
	c = Vector(s);

	xo = Vector(n, x);
	fxo = Vector(m, y);

	double* z = new double[s];
	zos_an_forward(tag, m, n, keep, x, y, z);

	double* rowvals = new double[n + s];
	for (size_t r = 0; r < m + s; r++) {
		fos_an_reverse(tag, m, n, s, r, rowvals);
		insertRow(r, rowvals);
	}

	Vector vz(s, z);
	c = vz - Z * xo - L * vz.abs();
	b = fxo - J * xo - Y * vz.abs();
}

void PlanC::updateLinearization(const Vector &in_x) {
	if (F) {
		double* x = new double[n];
		double* y = new double[m];

		for (size_t i = 0; i < n; ++i) {
			x[i] = in_x[i];
		}
		short tag = 0;
		F(tag, x, y);
		int keep = 1;
		double* z = new double[s];

		zos_an_forward(tag, m, n, keep, x, y, z);

		xo = Vector(n, x);
		fxo = Vector(m, y);

		zos_an_forward(tag, m, n, keep, x, y, z);

		double* rowvals = new double[n + s];
		for (size_t r = 0; r < m + s; r++) {
			fos_an_reverse(tag, m, n, s, r, rowvals);
			insertRow(r, rowvals);
		}
		factorized = false;

		Vector vz(s, z);
		c = vz - Z * xo - L * vz.abs();
		b = fxo - J * xo - Y * vz.abs();
	} else {
		xo = in_x;
		fxo = eval(in_x);
	}
}

void PlanC::insertRow(size_t row, double* in) {
	assert(row < s + n);
	Big.insertRow(row, in);
	if (row < s) {
		Z.insertRow(row, in);
		L.insertRow(row, in + n);
	} else {
		J.insertRow(row - s, in);
		Y.insertRow(row - s, in + n);
	}
}

void PlanC::factorize() {
//LU factorization after Crout

//Matrix A(J);

	Vector vv(n, false); //implicit scaling for each row
	double big, t, sum;
	size_t i, j, k, imax;

	Big.insertBlock(s, 0, J);

//get implicit scaling
	for (i = 0; i < n; ++i) {
		big = 0.0;
		for (j = 0; j < n; ++j) {
			if ((t = fabs(Big[s + i][j])) > big) {
				big = t;
			}
		}
		//TODO max == 0 -> singular matrix
		vv[i] = 1.0 / big;
	}

//loop over columns of Crout's method
	for (j = 0; j < n; ++j) {
		imax = j;
		//update U
		for (i = 0; i < j; ++i) {
			sum = Big[s + i][j];
			for (k = 0; k < i; ++k) {
				sum -= Big[s + i][k] * Big[s + k][j];
			}
			Big[s + i][j] = sum;
		}

		big = 0.0;
		for (i = j; i < n; i++) {
			sum = Big[s + i][j];
			for (k = 0; k < j; k++) {
				sum -= Big[s + i][k] * Big[s + k][j];
			}
			Big[s + i][j] = sum;
			if ((t = vv[i] * fabs(sum)) >= big) { //found bigger pivot
				big = t;
				imax = i;
			}
		}
		if (j != imax) {
			for (k = 0; k < n; k++) {
				swap(Big[s + imax][k], Big[s + j][k]);
			}
			vv[imax] = vv[j];
		}
		idx[j] = imax;
		//insert TINY here to catch A[j][j]== 0 singularity

		//multiply with pivot
		if (j != n - 1) {
			t = 1.0 / Big[s + j][j];
			for (i = j + 1; i < n; i++) {
				Big[s + i][j] *= t;
			}
		}

	}

//	cout << "J = " << J;
//	cout << "LU = " << SubMatrix(Big, s, 0, n, n);
//	cout << "idx = [" << idx << "]" << endl;
//	cout << "L = tril(LU)-diag(diag(LU));" << endl;
//	cout << "U = LU - L;" << endl;
//	cout << "L = L +eye(" << n << ");" << endl;
//	cout << "P = [0 0 1; 0 1 0; 1 0 0];" << endl;

//Calculate S = L - ZJ'Y
	SubMatrix YNew(Big, s, n, n, s);

	Vector v(n, false);
	for (j = 0; j < s; j++) { //find inverse
		for (i = 0; i < n; i++) {
			v[i] = Y[i][j];
		}
		v = linearSolveLU(LUJ, idx, v);
		//		cout << v << endl;
		for (i = 0; i < n; i++) {
			//Big[s+i][n+j] = v[i];
			YNew[i][j] = v[i];
		}
	}
	S = L - Z * YNew;

//TODO calc rest of Big

//	cout << "Big = " << Big;
//
//	cout << "Y = " << Y;
//	cout << "JY = " << SubMatrix(Big,s,n,n,s);
//	cout << "U\\(L\\(P*Y)) - JY";//test

	factorized = true;
}
bool PlanC::is_factorized() {
	return factorized;
}

Vector PlanC::calculate_z(const Vector &x) {
	Vector z(s, false);
//calculate z = c + Zx + L|z|
	for (size_t i = 0; i < s; i++) {
		z[i] = c[i];
		//Zx
		for (size_t j = 0; j < n; j++) {
			z[i] += Z[i][j] * x[j];
		}
		//Lz, L strictly lower triangle
		for (size_t j = 0; j < i; j++) {
			z[i] += L[i][j] * fabs(z[j]);
		}
	}
//cout << "calculate z\n" << "x= " << x << "\nz= " << z << "\n";
	return z;
}

Vector PlanC::calculate_x(Vector z, Vector y) {
//solve y = b + Jx + Y|z| for x
	if (!factorized) {
		factorize();
	};
	Vector t = y - Y * z.abs() - b;

	return linearSolveLU(LUJ, idx, t);
}

Vector PlanC::eval(const Vector z, const Vector x) {
	assert(z.length() == s);
	assert(x.length() == n);
	return b + J * x + Y * z.abs();
}
Vector PlanC::eval(const Vector &x) {
	Vector z = calculate_z(x);
	return b + J * x + Y * z.abs();
}

Vector PlanC::integrate(const Vector v) {
//integrates from 0 to 1/2
	assert(v.length() == n);
	Vector x(n);
// size_t k(0); //number of kinks
	Vector z = calculate_z(x + xo);
//cout << "this is integrate\n";
//cout << "z = " << z;
	Vector dy_old = eval(z, x + xo);
//cout << "dy = " << dy_old;
	Vector dy(m, false);
	Vector result(m);
	double tau(0);
	double tau_new;
	do {
		tau_new = 0.5;
		Vector dz(s);

		for (size_t i = 0; i < s; ++i) { //find next kink
			// Zv
			for (size_t j = 0; j < n; ++j) {
				dz[i] += Z[i][j] * v[j];
			}
			//Lsigma
			for (size_t j = 0; j < i; ++j) {
				if (z[j] != 0) {
					dz[i] += L[i][j] * (z[j] < 0 ? -1 : 1) * dz[j];
				} else {
					dz[i] += L[i][j] * fabs(dz[j]);
				}
			}
			if (-z[i] / dz[i] < tau_new && -z[i] / dz[i] > 0) {
				tau_new = -z[i] / dz[i];
			}
		}

		if (tau + tau_new > 0.5) { //check if kink relevant
			tau_new = 0.5 - tau;
			tau = 0.5;
		} else {
			tau += tau_new;
		}

		x = tau * v;
		z = calculate_z(x + xo);
		dy = eval(z, x + xo);
		dy_old += dy;
		dy_old *= tau_new * 0.5;
		result += dy_old;
		dy_old = dy;
	} while (tau < 0.5);

	return result;
}

Vector PlanC::integrate_simple(Vector v, const double h) {
	assert(v.length() == n);
	Vector x(n);
	Vector y(m);
	double tau = 0;
	while (tau < 0.5) {
		y += h * eval(x);
		tau += h;
		x = tau * v;
	}
	return y;
}

Vector PlanC::getCHat(const Vector &y) {
//calculate c_hat = c - ZJ'b = c - ZB'RA'b with ' = inverse
//ZB'R from S
	assert(y.length() == m);
	if (!factorized) {
		factorize();
	};

	Vector v = linearSolveLU(LUJ, idx, b - y);

	return c - Z * v;
}

Vector PlanC::findRoot(double TOL) {
	if (!factorized) {
		factorize();
	};
	Vector y(n);
	Vector c_hat = getCHat(y);
	double e = INFINITY;

	Vector z(s, false);
	Vector znew(s, false);
	for (size_t i = 0; i < s; ++i) {
		znew[i] = 1;
	}

	int cnt = 0;
	while ((e > TOL) && (cnt < MAX_IT)) {
		++cnt;
		z = S * znew.abs() + c_hat;
		e = norm(z - znew);
		znew = z;
		//std::cout << cnt << ".\t" << e << "\n";
	}

	return calculate_x(z, y);
}

Vector PlanC::solve(const Vector &y, size_t &cnt_it, double TOL, bool verbose) {
	if (!factorized) {
		factorize();
	};
	Vector c_hat = getCHat(y);

	double e = INFINITY;

	Vector z(s, false);

	Vector xnew(m);
	Vector x(m, false);

	if (verbose) {
		cout << "This is solve\n";
		cout << "solving for rhs " << y << "\n";
		cout << "it\tError\n";
	}
	cnt_it = 0;
	while ((e > TOL) && (cnt_it < MAX_IT)) {
		++cnt_it;
		x = xnew;
		z = calculate_z(x);
		//Tmp = I - S*Sigma
		Matrix Tmp = -SigmaMatrix(S, z);
		for (size_t i = 0; i < s; ++i) {
			Tmp[i][i] += 1;
		}
		z = linearSolve(Tmp, c_hat);
		//cout << "check linsolve = " << norm(Tmp * z - c_hat) << endl;
		xnew = calculate_x(z, y);
		e = norm(x - xnew);
//		e = error(x, xnew);
		if (verbose) {
			cout << cnt_it << ".\t" << e << "\n";
		}
	}
	if (cnt_it == MAX_IT) {
		cerr << "solve did not converge!\n";
	}
//	cout << "cnt = " << cnt << endl;

	return x;
}

double PlanC::crit_mult(const Vector &x, const Vector &dx, size_t crit_idx,
		const Vector &sig) {
	assert(sig.length() == s && dx.length() == n && n == x.length());

	Vector z = calculate_z(x);
	Vector dz(s);
	double sj;

	double tau_min = INFINITY;
	double t;

	for (size_t i = 0; i < s; ++i) {
		// Zv
		for (size_t j = 0; j < n; ++j) {
			dz[i] += Z[i][j] * dx[j];
		}

		//Lsigma wit external sigma and not sign(z)
		for (size_t j = 0; j < i; ++j) {
			if (sig[j] != 0) {
				dz[i] += L[i][j] * sig[j] * dz[j];
			} else {
				dz[i] += L[i][j] * fabs(dz[j]);
			}
		}

		t = -z[i] / dz[i];
		if (t < tau_min) {
			tau_min = t;
			crit_idx = i;
		}
	}
	return tau_min;
}

Matrix PlanC::gen_jac(const Vector &x, const Vector &v) {
//find max
	size_t k = 0;
	double max = 0;
	for (size_t i = 0; i < n; ++i) {
		if (fabs(v[i]) > max) {
			max = fabs(v[i]);
			k = i;
		}
	}

	Vector z = calculate_z(x);
	Vector sig(s);

//calculate dz
	Matrix gz(n, n);
	for (size_t i = 0; i < n; ++i) {
		if (0 != z[i])
			sig[i] = z[i] < 0 ? -1 : 1;

		//Z * dx
		for (size_t j = 0; j < n; ++j) {
			gz[i][0] += Z[i][j] * v[j];
			if (j < k)
				gz[i][j + 1] += Z[i][j];
			else if (j > k)
				gz[i][j] += Z[i][j];
		}
		//L sigma gz
		for (size_t j = 0; j < i; ++j) {
			double d = L[i][j] * sig[j];
			gz[i] += gz[j] * d;
		}

		//fill sigma (first sign)
		size_t j = 0;
		while (sig[i] == 0 && j < s) {
			if (0 != gz[i][j])
				sig[i] = gz[i][j] < 0 ? -1 : 1;
			++j;
		}
	}

//calculate gy
	Matrix gy(n, n);
	for (size_t i = 0; i < n; ++i) {
		//J * dx
		for (size_t j = 0; j < n; ++j) {
			gy[i][0] += J[i][j] * v[j];
			if (j < k)
				gy[i][j + 1] += J[i][j];
			else if (j > k)
				gy[i][j] += J[i][j];
		}
		// Y sigma gz
		for (size_t j = 0; j < n; ++j) {
			double d = Y[i][j] * sig[j];
			gy[i] += gz[j] * d;
		}
	}

// recover J
	Matrix jac(n, n, false);
	for (size_t j = 0; j < n; ++j) { //column of J
		for (size_t i = 0; i < k; ++i)
			jac[i][j] = gy[j][i + 1]; //a
		for (size_t i = k + 1; i < n; ++i)
			jac[i][j] = gy[j][i]; //c

		double beta = gy[j][0]; //alpha
		for (size_t i = 0; i < k; ++i)
			beta -= jac[i][j] * v[i]; //a * u
		for (size_t i = k + 1; i < n; ++i)
			beta -= jac[i][j] * v[i]; //c w

		jac[k][j] = beta / v[k];
	}

	return jac;
}

Matrix PlanC::jac(const Vector &x) {
	return J + Y * SigmaMatrix(Z, calculate_z(x));
}

int PlanC::firstsign(const double &z, Row gzj) {
	if (z != 0)
		return z < 0 ? -1 : 1;
	for (size_t i = 0; i < s; ++i) {
		if (gzj[i] != 0)
			return gzj[i] < 0 ? -1 : 1;
	}
	return 0;
}

Matrix PlanC::solve_ode(const double N_in, const Vector &x0, const double t0,
		const double t_end, bool verbose, double TOL, Vector &cnt_iterations) {
	size_t N(N_in);
	assert(x0.length() == n);
	assert(m == n);

	Matrix x(N, n, false);

	double h = (t_end - t0) / N;
	size_t cnt_inner_it;	//iteration counter

	size_t idx = 0;
	x[idx] = x0;
	Vector xc(n, false);
	Vector xo(n, false);
	Vector dxo(n, false);
	Vector r(n, false);
	Vector v(n, false);
	Vector zero(n);

	xc = x0;
	if (verbose) {
		cout << "t x" << endl;
		cout << t0 << " " << xc << endl;
	}

	Vector res(n, false);

	while (idx < N - 1) {
		++idx;
		xo = xc + h * 0.5 * eval(xc);
		size_t cnt_outer_it = 0;
		do {
			++cnt_outer_it;
			cnt_inner_it = 0;
			updateLinearization(xo);
			v = 2 * (xo - xc);
			r = h * (integrate(v) + integrate(-v) - fxo);
			PlanC tmp = linear_combination(2.0, -h);
			xo = tmp.solve(r + 2 * xc, cnt_inner_it, 1E-14);
			res = 2 * xo - 2 * xc - r - h * eval(xo);
			if (cnt_iterations.length() > 0) {
				cnt_iterations[idx] += cnt_inner_it;
			}
		} while (norm(res) > TOL && cnt_outer_it < MAX_IT);
//		} while (error(2 * xo - 2 * xc, r + h * eval(xo)) > TOL
//				&& cnt_inner_it < MAX_IT);
		if (cnt_outer_it == MAX_IT) {
			cerr
					<< "Maximal inner iterations reached. Consider stopping the program."
					<< endl;
//			cerr << "error is " << error(2 * xo - 2 * xc, r + h * eval(xo))
//					<< endl;
		}
		xc = 2 * xo - xc;

		if (verbose) {
			cout << t0 + idx * h << " " << xc << endl;
		}

		x[idx] = xc;
	}

	return x;
}

Matrix PlanC::solve_ode_midpoint_implicit(const double N_in, const Vector &x0,
		const double t0, const double t_end, bool verbose, double TOL,
		Vector &cnt_iterations) {
	size_t N(N_in);
	assert(x0.length() == n);
	assert(m == n);

	Matrix x(N, n, false);

	double h = (t_end - t0) / N;
	size_t cnt_inner_it;	//iteration counter
	size_t cnt_outer_it;	//iteration counter

	size_t idx = 0;
	x[idx] = x0;
	Vector xc(n, false);
	Vector xo(n, false);
	Vector dxo(n, false);
	Vector zero(n);

	xc = x0;
	if (verbose) {
		cout << "t x" << endl;
		cout << t0 << " " << xc << endl;
	}

	Vector res(n, false);

	while (idx < N - 1) {
		++idx;
		xo = xc + h * 0.5 * eval(xc);
		cnt_outer_it = 0;
		do {
			++cnt_outer_it;
			cnt_inner_it = 0;
			updateLinearization(xo);
			PlanC tmp = linear_combination(2.0, -h);
			xo = tmp.solve(2 * xc, cnt_inner_it, 1E-14);
			res = 2 * xo - 2 * xc - h * eval(xo);
			if (cnt_iterations.length() > 0) {
				cnt_iterations[idx] += cnt_inner_it;
			}
		} while (norm(res) > TOL && cnt_outer_it < MAX_IT);
//		} while (error(2 * xo - 2 * xc, r + h * eval(xo)) > TOL
//				&& cnt_inner_it < MAX_IT);
		if (cnt_outer_it == MAX_IT) {
			cerr
					<< "Maximal inner iterations reached. Consider stopping the program."
					<< endl;
//			cerr << "error is " << error(2 * xo - 2 * xc, r + h * eval(xo))
//					<< endl;
		}
		xc = 2 * xo - xc;

		if (verbose) {
			cout << t0 + idx * h << " " << xc << endl;
		}

		x[idx] = xc;
	}

	return x;
}

Matrix PlanC::solve_ode_midpoint_explicit(double N, const Vector &x0,
		double t_0, double t_end, bool verbose) {

	Matrix x(N, n, false);
	double h = (t_end - t_0) / N;

	size_t idx = 0;
	x[idx] = x0;

	Vector xc(x0);
	Vector tmp(n, false);
	if (verbose) {
		cout << "t x" << "\n";
		cout << "0 " << x0 << "\n";
	}

	while (idx < N - 1) {
		++idx;
		xc += h * eval_F(xc + 0.5 * h * eval_F(xc));
		if (verbose) {
			cout << idx * h << " " << xc << "\n";
		}
		x[idx] = xc;
	}

	return x;
}

Vector PlanC::eval_F(const Vector &x_in) {
	if (F) {
		double* x = new double[n];
		for (size_t i = 0; i < n; i++) {
			x[i] = x_in[i];
		}
		double* y = new double[m];

		F(0, x, y);

		return Vector(m, y);
	} else {
		return eval(x_in);
	}
}

void PlanC::plot(size_t N, double x_end) {
	double h = x_end / N * 0.5;

	if (F) {

		cout << "# h = " << h << endl;
		for (size_t i = 0; i < n; i++) {
			cout << "x" << i << " ";
		}
		for (size_t i = 0; i < m; i++) {
			cout << "y" << i << " ";
		}
		for (size_t i = 0; i < m; i++) {
			cout << "py" << i << " ";
		}
		cout << endl;

		Vector x(n, false);
		Vector ind(n);
		size_t j = 0;

		while (ind[n - 1] <= N) {
			x = xo;
			for (size_t i = 0; i < n; i++) {
				x[i] += ind[i] * h;
			}
			cout << x << " " << eval(x) << " " << eval_F(x) << endl;
			x = xo;
			for (size_t i = 0; i < n; i++) {
				x[i] -= ind[i] * h;
			}
			cout << x << " " << eval(x) << " " << eval_F(x) << endl;

			ind[0]++;
			j = 0;
			while (ind[j] == N && j < n - 1) {
				ind[j] = 0;
				ind[j + 1]++;
				j++;
			}
		}
	} else {
		cout
				<< "This method plots only linearizations for non piecewise linear functions"
				<< endl;
		return;
	}
}

void PlanC::plot(size_t N, double interval, size_t x_ind, size_t y_ind) {
	double h = interval / N * 2;

	cout << "# xo = " << xo[x_ind] << "\tfxo = " << fxo[y_ind] << endl;
	Vector x = xo;
	Vector y(m, false);
	Vector py(m, false);
	x[x_ind] -= interval;

	if (F) {
		cout << "x y py" << endl;
	} else {
		cout << "x py" << endl;
	}

	for (size_t i = 0; i <= N; i++) {
		if (F) {
			y = eval_F(x);
		}
		py = eval(x);
		if (F) {
			cout << x[x_ind] << " " << y[y_ind] << " " << py[y_ind] << endl;
		} else {
			cout << x[x_ind] << " " << py[y_ind] << endl;
		}
		x[x_ind] += h;
	}
}

PlanC PlanC::linear_combination(double alpha, double beta) {
	assert(n == m);
	PlanC result(*this);
	result.b *= beta;
	result.J *= beta;
//add diag alpha
	for (size_t i = 0; i < n; ++i) {
		result.J[i][i] += alpha;
	}
	result.Y *= beta;
	return result;
}

ostream& operator <<(ostream &os, const PlanC &p) {
	os << "\nPrinting System\n";
	os << "===============\n";
	os << "b = " << p.b << "\n";
	os << "c = " << p.c << "\n";
	os << "Z = " << p.Z << "\n";
	os << "L = " << p.L << "\n";
	os << "J = " << p.J << "\n";
	os << "Y = " << p.Y << "\n";
	if (p.factorized) {
		os << "Big = " << p.Big << "\n";
		os << "LUJ = " << p.LUJ << "\n";
	}
	return os;
}
