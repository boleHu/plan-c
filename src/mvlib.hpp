#ifndef MVLIB_H
#define MVLIB_H
#define CHECK_LIMITS

#include <iostream>
#include <assert.h>
#include <math.h>
#include <utility>
#include <type_traits>

//used standard elements
using std::false_type;
using std::true_type;
using std::enable_if;
using std::is_arithmetic;
using std::ostream;
using std::swap;

class Vector;
class Row;
class Matrix;
class SubMatrix;
template<typename T> struct is_vector_type: false_type {
};
//false by default
template<> struct is_vector_type<Vector> : true_type {
};
template<> struct is_vector_type<Row> : true_type {
};

template<typename T> struct is_matrix_type: false_type {
};
//false by default
template<> struct is_matrix_type<Matrix> : true_type {
};
template<> struct is_matrix_type<SubMatrix> : true_type {
};

class Row {
	friend class Vector;
private:
	double* data_;
	size_t length_;
public:
	Row(double* data, size_t length_);
	size_t length() const;
	Row &operator =(const Vector &other);
	double &operator [](size_t index);
	const double &operator [](size_t index) const;

//compound operators
	template<typename V>
	Row &operator +=(const V &other) {
		assert(length_ == other.length());
		for (size_t i = 0; i < length_; ++i) {
			data_[i] += other[i];
		}
		return *this;
	}
	;
	template<typename V>
	Row &operator -=(const V &other) {
		assert(length_ == other.length());
		for (size_t i = 0; i < length_; ++i) {
			data_[i] -= other[i];
		}
		return *this;
	}
	;
	Row &operator *=(const double &d);

};

class Vector {
	friend class Row;
private:
	double *data_;
	size_t length_;
public:
	Vector();
	Vector(size_t length, double* x_in);
	Vector(size_t length, bool initialized = true);
	Vector(const Vector &other); //copy : Vector v2(v);
	Vector(const Row &other);
	Vector(Vector &&other); //move : Vector v2 = foo();
	~Vector();

	Vector abs() const;
	size_t length() const;

	Vector &operator =(const Vector &other); //copy v2 = v;
	Vector &operator =(Vector &&other); //move
	double &operator [](size_t index); //array like access v[i]
	double const operator [](size_t index) const; //v[i] (for cout)

//compound operators
	template<typename V>
	Vector &operator +=(const V &other) {
		assert(length_ == other.length());
		for (size_t i = 0; i < length_; ++i) {
			data_[i] += other[i];
		}
		return *this;
	}
	;
	template<typename V>
	Vector &operator -=(const V &other) {
		assert(length_ == other.length());
		for (size_t i = 0; i < length_; ++i) {
			data_[i] -= other[i];
		}
		return *this;
	}
	;
	Vector &operator *=(const double &d);
	const Vector operator -() const;
	friend double err(const Vector &v, const Vector &w);
	void insertBlock(const Vector &v, size_t idx);
};

class Matrix {
	friend class SubMatrix;
private:
	double *data_;
	size_t rows_, columns_;
public:
	Matrix();
	Matrix(size_t rows, size_t columns, bool initialized = true);
	Matrix(const Matrix &other); //copy constructor: Matrix m2(m);
	Matrix(Matrix &&other);
	~Matrix();

	void insertRow(size_t row, double* in);

	void insertBlock(size_t row, size_t column, Matrix &block);
//inserts a block from matrix m starting from (r_in,c_in) of the size (r_n,c_in)
//into the calling matrix at (r_out, c_out)
	void insertBlock(size_t r_out, size_t c_out, Matrix &m, size_t r_in,
			size_t c_in, size_t r_n, size_t c_n);

	Row operator [](size_t row);
	const Row operator [](size_t row) const;
	Matrix &operator =(const Matrix &other);
	Matrix &operator =(Matrix &&other);

//compound operators
	template<typename M>
	Matrix &operator +=(const M &other) {
		assert(columns_ == other.getColumns());
		assert(rows_ == other.getRows());
		for (size_t i = 0; i < rows_; ++i) {
			for (size_t j = 0; j < columns_; ++j) {
				data_[i * columns_ + j] += other[i][j];
			}
		}
		return *this;
	}
	template<typename M>
	Matrix &operator -=(const M &other) {
		assert(columns_ == other.getColumns());
		assert(rows_ == other.getRows());
		for (size_t i = 0; i < rows_; ++i) {
			for (size_t j = 0; j < columns_; ++j) {
				data_[i * columns_ + j] -= other[i][j];
			}
		}
		return *this;
	}
	Matrix &operator *=(const double &d);

	const Matrix operator -() const;

	size_t getRows() const;
	size_t getColumns() const;
};

class SubMatrix {
private:
	size_t rows_, columns_, row_offset_, column_offset_;
	Matrix &matrix;
public:
	SubMatrix(Matrix &m, size_t row_offset, size_t column_offset, size_t rows,
			size_t columns);

	SubMatrix &operator =(const Matrix &other);
	SubMatrix &operator =(const SubMatrix &other);

	Row operator [](size_t row);
	const Row operator [](size_t row) const;
	template<typename M>
	SubMatrix &operator +=(const M &other) {
		assert(columns_ == other.getColumns());
		assert(rows_ == other.getRows());
		for (size_t r = 0; r < rows_; ++r) {
			for (size_t c = 0; c < columns_; ++c) {
				matrix.data_[(row_offset_ + r) * matrix.columns_
						+ column_offset_ + c] += other[r][c];
			}
		}
		return *this;
	}
	template<typename M>
	SubMatrix &operator -=(const M &other) {
		assert(columns_ == other.getColumns());
		assert(rows_ == other.getRows());
		for (size_t r = 0; r < rows_; ++r) {
			for (size_t c = 0; c < columns_; ++c) {
				matrix.data_[(row_offset_ + r) * matrix.columns_
						+ column_offset_ + c] -= other[r][c];
			}
		}
		return *this;
	}

	size_t getRows() const;
	size_t getColumns() const;
};

template<typename MatrixType, typename VectorType>
Vector linearSolve(MatrixType M, VectorType v) {
//	cout << "This is linear solve." << endl;
//	cout << "Solving for v = " << v << endl;
//	cout << "and M = " << M << endl;
	size_t n = M.getRows();
	assert(n == M.getColumns());
	assert(n == v.length());

	Vector result(n, false);
	double r, max, jmax;
//forward
	for (size_t i = 0; i < n; ++i) {
		max = fabs(M[i][i]);
		jmax = i;
		//find largest element in column
		for (size_t j = i + 1; j < n; ++j) {
			r = fabs(M[j][i]);
			if (r > max) {
				max = r;
				jmax = j;
			}
		}

		if (jmax != i) {
			swap(v[i], v[jmax]);
			for (size_t k = i; k < n; ++k) {
				swap(M[i][k], M[jmax][k]);
			}
		}

		for (size_t j = i + 1; j < n; ++j) {
			r = -M[j][i] / M[i][i];
			M[j][i] = 0;
			for (size_t k = i + 1; k < n; ++k) {
				M[j][k] += r * M[i][k];
			}
			v[j] += r * v[i];
		}
	}

	for (size_t i = 0; i < n; ++i) {
		result[n - i - 1] = v[n - i - 1];
		for (size_t j = n - i; j < n; ++j) {
			result[n - i - 1] -= M[n - i - 1][j] * result[j];
		}
		result[n - i - 1] /= M[n - i - 1][n - i - 1];
	}
	return result;
}

template<typename MatrixType, typename VectorType>
Vector linearSolveLU(const MatrixType M, const VectorType idx,
		const VectorType b_in) {
//see numerical recipes
	size_t i, ii = 0, ip, j;
	double sum;
	Vector b(b_in);

	size_t n = M.getRows();

	for (i = 0; i < n; i++) { //forward
		ip = size_t(idx[i]);
		sum = b[ip];
		b[ip] = b[i];
		if (ii != 0) {
			for (j = ii - 1; j < i; j++) {
				sum -= M[i][j] * b[j];
			}
		} else if (sum != 0.0) {
			ii = i + 1;
		}
		b[i] = sum;
	}

//for (i = n-1; i >= 0; i--) for unsigned
	for (i = n; i-- > 0;) { //backward
		sum = b[i];
		for (j = i + 1; j < n; j++) {
			sum -= M[i][j] * b[j];
		}
		b[i] = sum / M[i][i];
	}
	return b;
}

Matrix spline(Vector x_g, Matrix y_g, Vector x);
double error (Matrix x_big, Matrix x_small);

template<typename V>
double norm(const V &v) {
	size_t n = v.length();
	double r(0);
	for (size_t i = 0; i < n; ++i) {
		r += fabs(v[i]);
	}
	return r;
}


template<typename T>
double rel_error(T a, T b){
	if (a == 0 && b == 0 ){
		return 0;
	}
	return fmax(fabs((a-b)/a),fabs((a-b)/b));
}

template<typename V>
double error(const V &v1, const V &v2){
	size_t n = v1.length();
	assert(v2.length() == n);
	double r(0);
		for (size_t i = 0; i < n; ++i) {
			r += pow(rel_error(v1[i],v2[i]),2);
		}
		return sqrt(r)/n;
}

/*******************binary operators**************/
//Vector
template<typename V1, typename V2>
typename enable_if<is_vector_type<V1>::value && is_vector_type<V2>::value,
		const Vector>::type operator +(const V1 &lhs, const V2 &rhs) {
	Vector result(lhs);
	result += rhs;
	return result;
}
template<typename V1, typename V2>
typename enable_if<is_vector_type<V1>::value && is_vector_type<V2>::value,
		const Vector>::type operator -(const V1 &lhs, const V2 &rhs) {
	Vector result(lhs);
	result -= rhs;
	return result;
}
template<typename T1, typename T2>
typename std::enable_if<is_vector_type<T1>::value && is_arithmetic<T2>::value,
		const Vector>::type operator *(const T1 &t1, const T2 &t2) {
	Vector result(t1);
	result *= t2;
	return result;
}
template<typename T1, typename T2>
typename std::enable_if<is_vector_type<T2>::value && is_arithmetic<T1>::value,
		const Vector>::type operator *(const T1 &t1, const T2 &t2) {
	Vector result(t2);
	result *= t1;
	return result;
}

//Matrix
template<typename M1, typename M2>
typename enable_if<is_matrix_type<M1>::value && is_matrix_type<M2>::value,
		const Matrix>::type operator +(const M1 &lhs, const M2 &rhs) {
	Matrix result(lhs);
	result += rhs;
	return result;
}
template<typename M1, typename M2>
typename enable_if<is_matrix_type<M1>::value && is_matrix_type<M2>::value,
		const Matrix>::type operator -(const M1 &lhs, const M2 &rhs) {
	Matrix result(lhs);
	result -= rhs;
	return result;
}
template<typename M1, typename M2>
typename enable_if<is_matrix_type<M1>::value && is_matrix_type<M2>::value,
		const Matrix>::type operator *(const M1 &lhs, const M2 &rhs) {
	assert(lhs.getColumns() == rhs.getRows());
	Matrix result(lhs.getRows(), rhs.getColumns());
	for (size_t i = 0; i < lhs.getRows(); ++i) {
		for (size_t j = 0; j < rhs.getColumns(); ++j) {
			for (size_t k = 0; k < lhs.getColumns(); ++k) {
				result[i][j] += lhs[i][k] * rhs[k][j];
			}
		}
	}
	return result;
}
template<typename T1, typename T2>
typename std::enable_if<is_matrix_type<T1>::value && is_arithmetic<T2>::value,
		const Matrix>::type operator *(const T1 &t1, const T2 &t2) {
	Matrix result(t1);
	result *= t2;
	return result;
}
template<typename T1, typename T2>
typename std::enable_if<is_matrix_type<T2>::value && is_arithmetic<T1>::value,
		const Matrix>::type operator *(const T1 &t1, const T2 &t2) {
	Matrix result(t2);
	result *= t1;
	return result;
}

//Matrix Vector
template<typename M, typename V>
typename enable_if<is_matrix_type<M>::value && is_vector_type<V>::value,
		const Vector>::type operator *(const M &m, const V &v) {
	assert(m.getColumns() == v.length());
	Vector mv(m.getRows());
	for (size_t i = 0; i < m.getRows(); ++i) {
		for (size_t j = 0; j < m.getColumns(); ++j)
			mv[i] += m[i][j] * v[j];
	}
	return mv;
}

ostream& operator <<(ostream &os, const Vector &v);
ostream& operator <<(ostream &os, const Matrix &m);
ostream& operator <<(ostream &os, const SubMatrix &m);

//additional
template<typename MatrixType>
Matrix SigmaMatrix(const MatrixType &m, const Vector &z) {
	size_t s = z.length();
	assert(m.getRows() == s && m.getColumns() == s);
	Matrix result(s, s, false);
	double sign = 0;
	for (size_t i = 0; i < s; ++i) {
		for (size_t j = 0; j < s; ++j) {
			sign = z[j] == 0 ? 0 : (z[j] < 0 ? -1 : 1);
			result[i][j] = sign * m[i][j];
		}
	}
	return result;
}

static Vector __NULL_VECTOR__(0);

#endif
