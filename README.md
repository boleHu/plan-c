Install
====================

## STEP 0 (deprecated):
AdolC Git Repo:

`git://gitorious.org/adol-c/adol-c.git`

Installation:

* `clone git repo`
* cd to folder and type in terminal `autoreconf -fi`
* `./configure` 
* `make`
* `make install`

(Perhaps u need to install `sudo apt-get install autoconf`)

## STEP 0.5
Clone git Repo:

`git clone https://{username}@bitbucket.org/boleHu/plan-c.git`

## STEP1:

set Environment variable

* `nano ~/.bashrc` 
* `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/{GIT_FOLDER}/plan-c/lib/adolc/lib64`
* save
* `source ~/.bashrc`
* restart eclipse

Test, if ok:

* `cd ~/{GIT_FOLDER}/plan-c/Debug` (after build invoke)
* `ldd Plan-C`
	* if something like `libadolc.so.1 => /u/lenser/git/plan-c/lib/adolc/lib64/libadolc.so.1 (0x00007f96dca9f000)`
appears, you win!

## STEP2 (optional):

* Project -> Rightclick -> Properties -> C/C++ Build --> Settings
* Tool Settings -> GCC C++ Compiler -> Includes -> `"${workspace_loc:/${ProjName}/lib/adolc/include}"`
* Tool Settings -> GCC C++ Linker -> Libraries 
	* Library Search Path `"${workspace_loc:/${ProjName}/lib/adolc/lib64}"`
	* Libraries `adolc`

## Manual compiling

    g++ -I"{ADOLC-HEADER-FOLDER}" -c -std=c++11 mvlib.cpp
    g++ -I"{ADOLC-HEADER-FOLDER}" -c -std=c++11 planc.cpp
    g++ -I"{ADOLC-HEADER-FOLDER}" -c -std=c++11 testPLANC.cpp
    g++ -L"{ADOLC-LIB-FOLDER}" -o "Plan-C" mvlib.o planc.o testPLANC.o -ladolc

## FAQ

* Sometimes the `generate Makefile automatically` token is not set correctly in eclipse so you have to do
	* Rightclick on your project -> Properties -> C/C++ Build -> Generate Makefile Automatically


Have fun, yo!

